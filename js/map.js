function initMap() {
    var uluru = {lat: 41.877387, lng: -87.629922};
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 12, center: uluru});
    var marker = new google.maps.Marker({position: uluru, map: map});
}