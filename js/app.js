// smooth scroll - arrow

$('.btn-footer, .navbar-brand').click(function(){
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
});

// smooth scroll - menu

let allSections = [...document.getElementsByTagName('section')];
let sectionsInMenu = [];

for (let i = 0; i < allSections.length; i++) {
    if (allSections[i].dataset.menu != null)
        sectionsInMenu.push(allSections[i].dataset.menu)
}

for (let i = 0; i < sectionsInMenu.length; i++) {
    let classNavLinkSection = ".nav-link-" + sectionsInMenu[i];
    let classSection = "." + sectionsInMenu[i];

    $(classNavLinkSection).click(function(){
        $("html,body").stop().animate({
            scrollTop: $(classSection).offset().top
        }, 750);
    });
}

// slide-in animation

const offset = (section) => {
    return Math.floor(section.getBoundingClientRect().top)
}

const animationInSection = () => {
    allSections.forEach(section => {
        const elementOffset = offset(section);

        if (elementOffset < (screen.height - 100) && elementOffset >= -(screen.height - 100)) {
            allElements = [...section.getElementsByClassName("animate")];

            allElements.forEach(element => {
                if (element.dataset.animate == "left"){
                    element.id = "left-slide";
                }
                if (element.dataset.animate == "bottom") {
                    element.id = "bottom-slide";
                }
            })
        }
    })
}

window.addEventListener("scroll", animationInSection);